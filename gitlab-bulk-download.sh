#!/bin/bash
#Downloads Gitlab EE versions 16.0.0, 16.3.7, 16.7.7, 16.11.4, 17.1.0
wget --content-disposition https://packages.gitlab.com/gitlab/gitlab-ee/packages/el/8/gitlab-ee-16.0.0-ee.0.el8.x86_64.rpm/download.rpm
wget --content-disposition https://packages.gitlab.com/gitlab/gitlab-ee/packages/el/8/gitlab-ee-16.3.7-ee.0.el8.x86_64.rpm/download.rpm
wget --content-disposition https://packages.gitlab.com/gitlab/gitlab-ee/packages/el/8/gitlab-ee-16.7.7-ee.0.el8.x86_64.rpm/download.rpm
wget --content-disposition https://packages.gitlab.com/gitlab/gitlab-ee/packages/el/8/gitlab-ee-16.11.4-ee.0.el8.x86_64.rpm/download.rpm
wget --content-disposition https://packages.gitlab.com/gitlab/gitlab-ee/packages/el/8/gitlab-ee-17.1.0-ee.0.el8.x86_64.rpm/download.rpm
curl -LJO "https://s3.dualstack.us-east-1.amazonaws.com/gitlab-runner-downloads/latest/deb/gitlab-runner_amd64.deb"
curl -LJO "https://s3.dualstack.us-east-1.amazonaws.com/gitlab-runner-downloads/latest/rpm/gitlab-runner_amd64.rpm"